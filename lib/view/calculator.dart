import 'package:SimpleCalculator/widget/calculator_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Calculator extends StatefulWidget {
  @override
  _CalculatorState createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  String _expression;
  String _input;

  @override
  void initState() {
    super.initState();
    _expression = '';
    _input = '';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: body(),
    );
  }

  Widget body() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        // Screen
        Expanded(
          flex: 2,
          child: Container(
            child: Text(_input),
            //color: Colors.teal,
          ),
        ),
        // Pad
        inputPad(),
      ],
    );
  }

  void userInput(String tap) => setState(() {
        if (_input.length >= 12) {
          return;
        }
        if (tap == 'C') {
          _input = '';
          return;
        }
        _input += tap;
      });

  /// Generate the input pad
  Widget inputPad() => Expanded(
        flex: 3,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              flex: 4,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  // NumPad
                  NumPad(userInput: userInput),
                  // ExpressionPad with expressions: +, -, *, /
                  ExpressionPad(userInput: userInput),
                ],
              ),
            ),
          ],
        ),
      );
}

class ExpressionPad extends StatelessWidget {
  final Function(String tap) userInput;

  const ExpressionPad({
    Key key,
    this.userInput,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        color: Colors.grey.shade700,
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Wrap(
            direction: Axis.vertical,
            spacing: 8,
            children: <Widget>[
              ..._expressions().map(
                    (e) => CalculatorButton(value: e, userInput: userInput),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _expressions() => ['C', '/', '*', '-', '+'];
}

class NumPad extends StatelessWidget {
  _numbers() => ['9', '8', '7', '6', '5', '4', '3', '2', '1', '.', '0'];

  final Function(String tap) userInput;

  const NumPad({
    this.userInput,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
        color: Colors.red,
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Wrap(
            direction: Axis.horizontal,
            textDirection: TextDirection.rtl,
            children: <Widget>[
              ..._numbers().map(
                    (e) => CalculatorButton(value: e, userInput: userInput),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
