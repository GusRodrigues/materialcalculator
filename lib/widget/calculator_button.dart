import 'package:flutter/material.dart';

class CalculatorButton extends StatelessWidget {
  final String value;

  const CalculatorButton({
    Key key,
    @required this.value,
    @required this.userInput,
  }) : super(key: key);

  final Function(String tap) userInput;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.black,
      highlightColor: Colors.white54,
      onPressed: () => userInput(value),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          value,
          style: TextStyle(
            color: Colors.white70,
            fontSize: 48,
            fontWeight: FontWeight.w300,
          ),
        ),
      ),
    );
  }
}
